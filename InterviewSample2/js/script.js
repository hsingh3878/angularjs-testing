var app = angular.module("MyApp", []);

app.controller("MyCtrl", ["$scope", "$timeout", "$interval", function($scope, $timeout, $interval) {
    $scope.message = 'Hello World';
    
    //variables to store status of all lights. It also initialises the status.
    var statusOfRedLight = true, statusOfYellowLight = false, statusOfGreenLight = false;
    
    //method to update the directive of red light
    $scope.isDisplayRed = function () {
        return statusOfRedLight;
    }
    
    //method to update the directive of yellow light
    $scope.isDisplayYellow = function () {
        return statusOfYellowLight;
    }

    //method to update the directive of green light
    $scope.isDisplayGreen = function () {
        return statusOfGreenLight;
    }
    
  
    //method to update the status of red light variable
    $scope.startRedLight = function (status) {
        statusOfRedLight = status;
        return statusOfRedLight;
    }

    //method to update the status of yellow light variable
    $scope.startYellowLight = function (status) {
        statusOfYellowLight = status;
        return statusOfYellowLight;
    }

    //method to update the status of green light variable
    $scope.startGreenLight = function (status) {
        statusOfGreenLight = status;
        return statusOfGreenLight;
    }
    
    //timer to hide red light and show yellow light
    $timeout( function(){ 
        $scope.startRedLight(false);
        $scope.startYellowLight(true);
        $scope.isDisplayYellow();
        $scope.isDisplayRed();

    }, 5000);
    
    //timer to hide yellow light and show green light
    $timeout( function(){
        $scope.startYellowLight(false);
        $scope.startGreenLight(true);
        $scope.isDisplayYellow();
        $scope.isDisplayGreen();
    }, 7000);
    
    //loop to keep the lights working 
    $interval( function(){ 
        
        //this is used to hide green light and show red light
        $scope.startGreenLight(false);
        $scope.startRedLight(true);
        $scope.isDisplayGreen();
        $scope.isDisplayRed();
        
        //timer to hide red light and show yellow light
        $timeout( function(){ 
            $scope.startRedLight(false);
            $scope.startYellowLight(true);
            $scope.isDisplayYellow();
            $scope.isDisplayRed();
            
        }, 5000);
        
        //timer to hide yellow light and show green light
        $timeout( function(){
            $scope.startYellowLight(false);
            $scope.startGreenLight(true);
            $scope.isDisplayYellow();
            $scope.isDisplayGreen();
        }, 7000);
    }, 12000);
    
}]);
